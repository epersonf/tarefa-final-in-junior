import React, { useState } from 'react';
import './Produtos.css';
import InputField from './InputField/InputField';
import MostradorProdutos from '../../components/OutrosComponentes/MostradorDeProdutos/MostradorDeProdutos';
import ViradorDePagina from '../../components/OutrosComponentes/ViradorDePagina/ViradorDePagina';

export default function Produtos() {
    const [actualPage, setActualPage] = useState(1);
    const [productsAmount, setProductsAmount] = useState(0);
    const [filter, setFilter] = useState(
        {
            'name': "",
            'category_id': -1,
            'min_price': 0,
            'max_price': 0
        }
    );


    const elementsPerPage = 10;
    const amountOfPages = Math.ceil(productsAmount/parseFloat(elementsPerPage));
    return (
        <React.Fragment>
            <InputField filter={filter} setFilter={setFilter}/>
            <MostradorProdutos filter={filter} setProductsAmount={setProductsAmount} actualPage={actualPage} elementsPerPage={elementsPerPage}  />
            <ViradorDePagina actualPage={actualPage} setActualPage={setActualPage} maxPageToShow={5} totalAmountOfPages={amountOfPages}/>
        </React.Fragment>
    )
}
