import React, { useState } from 'react';
import './InputField.css';
import CategorySelector from '../../../components/OutrosComponentes/CategorySelector/CategorySelector';

export default function Inputfield(props) {

    const [param, setParam] = useState(
        {
            'name': "",
            'category_id': -1,
            'min_price': 0,
            'max_price': 0
        }
    );

    return (
        <div className="input-field-produtos">
            <h1>Pesquise pelo que deseja:</h1>
            <label>
                Nome do produto:  
                <input value={param.name} onChange={(e) => setParam(
                    {
                        ...param,
                        'name': e.target.value
                    }
                )} type={"text"}/>
            </label>
            <div>
                <label>
                    Categoria do produto:  
                    <CategorySelector value={param.category_id} setCategory={(e) => {
                        setParam(
                            {
                                ...param,
                                'category_id': parseInt(e)
                            }
                        )
                    }}/>
                </label>
                <div>
                    <label>
                        <p>Amplitude de preço</p>
                        <input value={param.min_price} min={0} onChange={(e) => setParam(
                            {
                                ...param,
                                "min_price": parseFloat(e.target.value)
                            }
                        )} type={"number"}/>
                        <input value={param.max_price} min={0} onChange={(e) => setParam(
                            {
                                ...param,
                                'max_price': parseFloat(e.target.value)
                            }
                        )} type={"number"}/>
                    </label>
                </div>
            </div>
            <button onClick={() => props.setFilter(param)}>Aplicar</button>
        </div>
    )
}
