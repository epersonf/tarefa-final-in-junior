import React from 'react';
import './PaginaInicial.css';
import './SecaoChamativa/SecaoChamativa';
import SecaoChamativa from './SecaoChamativa/SecaoChamativa';
import ArticleShow from '../../components/OutrosComponentes/ArticleShow/ArticleShow';
import SecaoMostruario from './SecaoMostruario/SecaoMostruario';

export default function Paginainicial() {

    return (
        <div>
            <SecaoChamativa/>
            <SecaoMostruario/>
            <ArticleShow id={1}/>
        </div>
    );
}
