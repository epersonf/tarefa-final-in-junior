import React from 'react';
import './SecaoMostruario.css';
import MostradorDeProdutos from '../../../components/OutrosComponentes/MostradorDeProdutos/MostradorDeProdutos';

export default function Secaomostruario() {
    return (
        <section className="sctn-ms-vndds">
            <h3>Produtos mais vendidos</h3>
            <MostradorDeProdutos actualPage={1} elementsPerPage={3} sortMethod={(a, b) => b.sold_count - a.sold_count}/>
        </section>
    )
}
