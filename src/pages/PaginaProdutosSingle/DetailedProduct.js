import React, { useEffect, useState, useCallback } from 'react';
import './DetailedProduct.css';
import LikeButton from '../../components/OutrosComponentes/LikeButton/LikeButton';
import { fetchData } from '../../ConnectionManager';
import Carousel from '../../components/OutrosComponentes/Carousel/Carousel';
import { GlobalData, BakeGlobalData } from '../../VariableManager';

export default function ProdutoDetalhado(props) {
    const product = props.product.product;
    //#region Cart manipulations
    let cart = GlobalData['cart'];
    
    const isInCart = useCallback(() => {
        if (product === undefined) return;
        for (let i in cart) {
            if (product.id === cart[i].product.id) return i;
        }
        return -1;
    }, [cart, product]);

    const addToCart = () => {
        if (product === undefined) return;
        if (isInCart() !== -1) return;
        cart.push({
            'product': product,
            'amount': 1
        });
        setIndex(cart.length - 1);
        BakeGlobalData();
    }

    const removeFromCart = () => {
        if (isInCart() === -1) return;
        cart.splice(index, 1);
        setIndex(-1);
    }
    //#endregion

    const [index, setIndex] = useState(isInCart());

    useEffect(() => {
        setIndex(isInCart());
    }, [setIndex, isInCart]);

    const [category, setCategory] = useState(-1);

    const getCategory = () => {
        if (product === undefined) return;
        fetchData('categories/' + product.category_id, (e) => setCategory(e.name));
    };
    
    useEffect(getCategory, [props.product]);
    
    if (product === undefined) return (<React.Fragment></React.Fragment>);

    return(
        <section key={product.id} className="productDetails">
            <div>
                <Carousel imgs={product.images}/>
                <div className="productContainer">
                    <div className="mainInformations">
                        <h4>{product.name}</h4>
                        <p>{category}</p>
                        <LikeButton path={""} likeCount={product.like_count}/>
                        <p>Quantidade em estoque: {product.storage}</p>
                    </div>
                    
                    <div className="buyArea">
                        <h3>R$ {product.price}</h3>
                        <button className="red-btn" onClick={() => {
                            if (index === -1 || index === undefined) addToCart();
                            else removeFromCart();
                        }}>{index === -1 || index === undefined ? 'Adicionar' : 'Remover do carrinho'}</button>
                        <button className="green-btn" onClick={() => {
                            addToCart();
                            document.location.href = '/carrinho';
                        }}>Comprar agora</button>
                    </div>
                </div>
                <p className="product-description-detail">{product.description}</p>
            </div>
        </section>
    )
}