import React, {useState, useEffect} from 'react';
import MostradorDeComentarios from './Comentarios';
import DetailedProduct from './DetailedProduct';
import { fetchData } from '../../ConnectionManager';

export default function ProdutosSingle() {
    let querySplit = document.location.href.split('/');
    let productId = parseInt(querySplit[querySplit.length - 1]);
    if (isNaN(productId)) document.location.href = "/error";

    const [product, setProduct] = useState([]);

    const getProducts = (productId) => fetchData("products/" + productId, (e) => setProduct(e));

    useEffect(() => getProducts(productId), []);

    return (
        <div className="produtos-single-div">
            <DetailedProduct product={product}/>
            <MostradorDeComentarios product={product}/>
        </div>
    )
}
