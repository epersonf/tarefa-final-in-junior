import React from 'react';
import './Comentarios.css';
import LikeButton from '../../components/OutrosComponentes/LikeButton/LikeButton';

export default function MostradorDeComentarios(props) {

    const getComments = () => {
        return [
            {
                "id": 0,
                "product": "tenis",
                "nome": "Comment1",
                "comentario": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend euismod dolor, convallis convallis turpis tincidunt varius. Aenean vehicula quam sem, sed imperdiet elit fermentum sit amet.",
                "likes": 340
            },
            {
                "id": 1,
                "product": "tenis",
                "nome": "Comment2",
                "comentario": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend euismod dolor, convallis convallis turpis tincidunt varius. Aenean vehicula quam sem, sed imperdiet elit fermentum sit amet.",
                "likes": 340,
            },
            {
                "id": 2,
                "product": "tenis",
                "nome": "Comment3",
                "comentario": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend euismod dolor, convallis convallis turpis tincidunt varius. Aenean vehicula quam sem, sed imperdiet elit fermentum sit amet.",
                "likes": 340
            },
            {
                "id": 3,
                "produto": 4,
                "nome": "Comment4",
                "comentario": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend euismod dolor, convallis convallis turpis tincidunt varius. Aenean vehicula quam sem, sed imperdiet elit fermentum sit amet.",
                "likes": 340
            }
        ];
    }

    let comments = getComments();
    let commentsList = [];

    comments.forEach(e => {
        commentsList.push(
            <div key={e.id} className="commentsBox">
                <div>
                    <h4>{e.nome}</h4>
                    <LikeButton path={""} likeCount={e.likes}/>
                </div>
                <p>{e.comentario}</p>
            </div>
        );
    });

    return (
        <div className="show-comments">
            <form action="" className="commentsBox" id="input">
                <section>
                    <h4>Avalie este produto:</h4>
                    <textarea type="text" />
                    <button>Enviar</button>
                </section>
            </form>
            <section>
                {commentsList}
            </section>
        </div>
    )
}