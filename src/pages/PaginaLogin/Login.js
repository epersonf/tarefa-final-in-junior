import React, { useState } from 'react';
import './Login.css';
import { GlobalData, BakeGlobalData, ResetGlobalData } from '../../VariableManager';

export default function Login() {
    if (GlobalData['logged']) document.location.href = '/';

    const api = GlobalData['api'];
    
    const [email, setEmail] = useState("");
    const [password, setPsswrd] = useState("");

    const login = async () => {
        let response = await api.post("login",
            {
                "user": {
                    "email": email,
                    "password": password
                }
            },
        GlobalData['config']).catch((e) => {
            alert("Falha de autenticacao.");
        });

        if (response === undefined) return;

        let responseData = response.data;
        ResetGlobalData();
        GlobalData['logged'] = true;
        GlobalData['config']['headers']['Authorization'] = responseData.token;
        GlobalData['config_form']['headers']['Authorization'] = responseData.token;
        GlobalData['user'] = responseData.user;
        BakeGlobalData();
        document.location.href = "/";
    }

    return (
        <section className="login-sctn">
            <div>
                <h1>Logue em sua conta</h1>
                <label>Email</label>
                <input value={email} onChange={(e) => setEmail(e.target.value)} type="email"/>
                <label>Senha</label>
                <input value={password} onChange={(e) => setPsswrd(e.target.value)} type="password"/>
                <button id="btnBege" onClick={login}>Logar</button>
            </div>
        </section>
    )
}
