import React, { useState } from 'react';
import './Dashboard.css';
import { Route } from "react-router";
import { BrowserRouter } from 'react-router-dom';
import { GlobalData } from '../../VariableManager';
import SideMenu from './Subpaginas/Menu/SideMenu';

//#region Import SubPages
import ProdutosECategorias from './Subpaginas/ProdutosECategorias/ProdutosECategorias';
import Usuarios from './Subpaginas/Usuarios/Usuarios';
import Estoque from './Subpaginas/Estoque/Estoque';
import Administracao from './Subpaginas/Administracao/Administracao';
//#endregion

export default function Dashboard() {

    if (!GlobalData['user'].admin) document.location.href = '/';

    const [page, setPage] = useState(0);
    
    const pages = [ProdutosECategorias, Usuarios, Estoque, Administracao];

    return (
        <section className="dashboard">
            <SideMenu page={page} setPage={setPage}/>
            <BrowserRouter>
                <Route component={pages[page]}/>
            </BrowserRouter>
        </section>
    )
}
