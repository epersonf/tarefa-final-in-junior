import React from 'react';
import './Administracao.css';
import SoldGraph from './SoldGraph/SoldGraph';
import MostSoldGraph from './MostSoldGraph/MostSoldGraph';
import ArticleEditor from './ArticleEditor/ArticleEditor';

export default function Administracao() {

    
    return (
        <section className="adm-tab-dashboard">
            <SoldGraph />
            <MostSoldGraph amount={5}/>
            <ArticleEditor />
        </section>
    )
}
