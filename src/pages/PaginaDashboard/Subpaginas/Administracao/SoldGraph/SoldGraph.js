import './SoldGraph.css';
import { Doughnut } from 'react-chartjs-2';

import React, { useEffect, useState } from 'react';
import { generateRandomColor } from '../../../../../Functions';
import { fetchData } from '../../../../../ConnectionManager';

export default function SoldGraph() {

    const [catData, setCatData] = useState([]);

    useEffect(
        () => fetchData("statistics/sold_sum_by_category", setCatData)
    , []);

    let dataValues = [];
    let categoryNames = [];
    let backgroundColors = [];
    catData.forEach((e) => {
            dataValues.push(e.sold_sum);
            categoryNames.push(e.category_name);
            backgroundColors.push(generateRandomColor());
        }
    );

    let data = {
        datasets: [
            {
                data: dataValues,
                backgroundColor: backgroundColors
            }
        ],
        
        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: categoryNames
    };

    return (
        <div className="class-graph-sold">
            <Doughnut data={data}/>
        </div>
    )
}
