import React, { useState, useEffect } from 'react';
import './MostSoldGraph.css';
import { Doughnut } from 'react-chartjs-2';
import { fetchData } from '../../../../../ConnectionManager';
import { generateRandomColor, clamp } from '../../../../../Functions';

export default function MostSoldGraph(props) {

    const [catData, setCatData] = useState([]);

    const getData = () => fetchData('products', (e) => setCatData(e.products));

    useEffect(getData, []);

    let productNames = [];
    let dataValues = [];
    let backgroundColors = [];
    let count = 0;
    catData.forEach((e) => {
            count++;
            if (count > props.amount) return;
            productNames.push(e.name);
            dataValues.push(e.sold_count);
            backgroundColors.push(generateRandomColor());
        }
    );

    let data = {
        datasets: [
            {
                data: dataValues,
                backgroundColor: backgroundColors
            }
        ],
        
        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: productNames
    };

    return (
        <div className="canvas-mst-sld-grph">
            <Doughnut data={data} options={
                {
                    responsive: true,
                    maintainAspectRatio: true
                }
            } title="Mais vendidos" color="#0066cc"/>
        </div>
    );
}
