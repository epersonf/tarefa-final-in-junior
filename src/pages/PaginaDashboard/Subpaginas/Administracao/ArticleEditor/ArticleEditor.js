import React, { useState } from 'react';
import ArticleSelector from '../../../../../components/OutrosComponentes/ArticleSelector/ArticleSelector';
import ArticleEditorInput from '../../../../../components/OutrosComponentes/ArticleEditorInput/ArticleEditorInput';

export default function ArticleEditor() {

    const [articleId, setArticleId] = useState(1);

    const onChangeArticle = (e) => setArticleId(e);

    return (
        <section>
            <ArticleSelector changeCallBack={onChangeArticle} />
            <ArticleEditorInput articleId={articleId}/>
        </section>
    )
}
