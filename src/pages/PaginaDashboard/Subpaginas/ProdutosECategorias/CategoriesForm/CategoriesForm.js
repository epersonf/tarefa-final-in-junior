import React, { useState, useEffect } from 'react'
import CategoryCreator from './CategoryCreator/CategoryCreator'
import { GlobalData } from '../../../../../VariableManager';
import './CategoriesForm.css';
import ViradorDePagina from '../../../../../components/OutrosComponentes/ViradorDePagina/ViradorDePagina';
import { clamp, generateHtml } from '../../../../../Functions';
import { fetchData } from '../../../../../ConnectionManager';

export default function CategoriesForm() {
    const api = GlobalData['api'];
    
    const elementsPerPage = 3;
    
    const [actualPage, setActualPage] = useState(1);
    const [selectedId, setSelectedId] = useState(-1);
    const [categories, setCategories] = useState([]);

    const getCategories = () => fetchData('categories', (e) => {
        setCategories(e.sort((a, b) => a.id - b.id));
    });

    useEffect(getCategories, []);

    
    let start = elementsPerPage * (actualPage - 1);
    let end = clamp(start + elementsPerPage, 0, categories.length);

    const amountOfPages = Math.ceil(categories.length/parseFloat(elementsPerPage));
    return (
        <section className="categories-form">
            {generateHtml(categories, (e) => {
                return (
                    <div key={e.id} className="category-box">
                        <p>{e.name}</p>
                        <div>
                            <button onClick={
                                () => (e.id === selectedId) ? setSelectedId(-1) : setSelectedId(e.id)
                            }>Editar</button>
                            <button onClick={
                                () => api.delete('categories/' + e.id, GlobalData['config']).then(getCategories)
                            }>Deletar</button>
                        </div>
                    </div>
                );
            }, start, end)}
            <ViradorDePagina actualPage={actualPage} setActualPage={setActualPage} maxPageToShow={elementsPerPage} totalAmountOfPages={amountOfPages}/>
            <CategoryCreator selectedId={selectedId} updateCallback={getCategories}/>
        </section>
    )
}
