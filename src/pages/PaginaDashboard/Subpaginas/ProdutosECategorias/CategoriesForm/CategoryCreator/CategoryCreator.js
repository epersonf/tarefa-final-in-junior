import React, { useState } from 'react'
import { GlobalData } from '../../../../../../VariableManager';

export default function CategoryCreator(props) {
    const api = GlobalData['api'];

    const [categoryName, setCategoryName] = useState('');

    const submit = () => {
        if (props.selectedId !== -1) {
            api.put('categories/' + props.selectedId, {
                "name": categoryName
            }, GlobalData['config']).then(props.updateCallback)
        } else {
            api.post('categories', {
                "name": categoryName
            }, GlobalData['config']).then(props.updateCallback)
        }
    }

    // console.log(props.id);

    return (
        <div className="category-creator">
            <p>{props.selectedId === -1 ? "Crie sua categoria" : "Editando categoria de ID " + props.selectedId}</p>
            <input value={categoryName} onChange={(e) => setCategoryName(e.target.value)} type="text" />
            <button onClick={submit}>Enviar</button>
        </div>
    )
}
