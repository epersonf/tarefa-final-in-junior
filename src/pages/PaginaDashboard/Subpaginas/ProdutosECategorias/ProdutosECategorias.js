import React, { useState } from 'react';
import './ProdutosECategorias.css';
import ProdutoForm from './ProdutoForm/ProdutoForm';
import SeletorProdutos from './SeletorProdutos/SeletorProdutos';
import CategoriesForm from './CategoriesForm/CategoriesForm';

export default function ProdutosECategorias() {
    const [editingProduct, setEditingProduct] = useState(-1);
    return (
        <section className="prdts-ctgrs">
            <ProdutoForm editingProduct={editingProduct} setEditingProduct={setEditingProduct}/>
            <SeletorProdutos editingProduct={editingProduct} setEditingProduct={setEditingProduct} />
            <CategoriesForm/>
        </section>
    )
}
