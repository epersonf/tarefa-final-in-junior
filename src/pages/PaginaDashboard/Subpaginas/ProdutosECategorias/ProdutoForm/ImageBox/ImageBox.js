import React from 'react'
import './ImageBox.css';
import { GlobalData } from '../../../../../../VariableManager';

export default function ImageBox(props) {
    const api = GlobalData['api'];
    const image = props.imageInfo;

    const deleteImgBox = () => {
        let copyOfImg = [...props.imgs];
        let i;
        for (i = 0; i < copyOfImg.length; i++) {
            if (copyOfImg[i].url === image.url) break;
        }

        copyOfImg.splice(i, 1);
        props.setImgs(copyOfImg);
        if (image.id === -1) window.URL.revokeObjectURL(image.url);
        else {
            console.log("delete");
            api.delete('/products/' + props.productId + '/images/' + image.id);
        }
    }

    return (
        <div className="img-box">
            <button onClick={deleteImgBox}>X</button>
            <img src={image.url} alt="Imagem de upload" />            
        </div>
    )
}
