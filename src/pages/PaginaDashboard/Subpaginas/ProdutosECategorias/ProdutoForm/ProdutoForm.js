import React, { useState, useEffect } from 'react';
import './ProdutoForm.css';
import ImageBox from './ImageBox/ImageBox';
import CategorySelector from '../../../../../components/OutrosComponentes/CategorySelector/CategorySelector';
import { GlobalData } from '../../../../../VariableManager';
import { fetchData } from '../../../../../ConnectionManager';
import { mapFilter } from '../../../../../Functions';

const defaultBody = {
    "id": -1,
    "name": "",
    "category_id": -1,
    "description": "",
    "price": 5,
    "weight": 1,
    "width": 1,
    "height": 1,
    "depth": 1
};

export default function ProdutoForm(props) {
    const api = GlobalData['api'];

    const [formValues, setFormValues] = useState({...defaultBody});
    
    //#region Get de images
    const [imgs, setImgs] = useState([]);
    

    const getImg = (e) => {
        const file = e.target.files[0];
        const url = window.URL.createObjectURL(file);
        setImgs(
            imgs.concat(
                {
                    'id': -1,
                    'file': file,
                    'url': url
                }
            )
        );
    }
    //#endregion

    //#region Update form
    useEffect(() => {

        if (props.editingProduct.id > 0) {
            setFormValues(props.editingProduct);
            let newImgs = [];
            fetchData('products/' + props.editingProduct.id, (e) => {
                const product = e.product;
                product.images.push(product.thumbnail);
                newImgs = product.images;
                setImgs(newImgs);
            });
        } else {
            setFormValues({...formValues, 'id': -1});
            imgs.forEach(e => window.URL.revokeObjectURL(e.url));
            setImgs([]);
        }

    }, [props]);
    //#endregion
    
    //#region Construcao de HTML
    return (
        <div className="product-form">
            <h3>{(formValues.id <= 0) ? "Criar produto" : "Editando produto de ID: " + formValues.id}</h3>
            <div>
                <label>Nome do produto:</label>
                <input value={formValues['name']} onChange={(e) => setFormValues(
                    {
                        ...formValues,
                        'name': e.target.value
                    }
                )} type="text"/>
            </div>
            
            <div>
                <label>Categorias:</label>
                <CategorySelector value={formValues['category_id']} setCategory={(e) => setFormValues(
                    {
                        ...formValues,
                        'category_id': e
                    }
                )}/>
            </div>

            <div>
                <label>Descricao do produto</label>
                <textarea value={formValues['description'] || ''} onChange={(e) => setFormValues(
                    {
                        ...formValues,
                        'description': e.target.value
                    }
                )} type="text"/>
            </div>
            
            <div>
                <label>Preço:</label>
                <input value={formValues['price'] || ''} onChange={(e) => setFormValues(
                    {
                        ...formValues,
                        'price': e.target.value
                    }
                )} type="number" min={0} step="any"/>
            </div>

            <div>
                <label>Peso em G:</label>
                <input value={formValues['weight'] || 3.5} onChange={(e) => setFormValues(
                    e.target.value)} type="number" min={0} step="any"/> 
            </div>

            <div>
                <label>Dimensoes:</label>
                <div className="dimensions">
                    <input value={formValues['width'] || 1} onChange={(e) => setFormValues(
                        {
                            ...formValues,
                            'width': e.target.value
                        }
                    )} type="number" min={0} step="any"/> 
                    <input value={formValues['height'] || 1} onChange={
                        (e) => setFormValues(
                            {
                                ...formValues,
                                'height': e.target.value
                            }
                        )
                    } type="number" min={0} step="any"/> 
                    <input value={formValues['depth'] || 1} onChange={(e) => 
                        setFormValues(
                            {
                                ...formValues,
                                'depth': e.target.value
                            }
                        )
                    } type="number" min={0} step="any"/> 
                </div>
            </div>
            
            <label className="send-file-trigger" htmlFor="send-file-btn">
                Adicionar imagem
            </label>
            <input id="send-file-btn" type="file" onChange={getImg} />

            <div className="imgs-div">
                {
                    imgs.map((e, i) => {
                        return (<ImageBox key={i} imgs={imgs} productId={formValues.id} setImgs={setImgs} imageInfo={e}/>);
                    })
                }
            </div>

            <input onClick={
                async () => {
                    if (formValues.category_id === -1) {
                        alert("Voce deve selecionar uma categoria...");
                        return;
                    }

                    const method = (formValues.id <= 0) ? api.post : api.put;
                    const path = (formValues.id <= 0) ? 'products' : 'products/' + formValues.id;

                    let formData = new FormData();
                    formData.append('name', formValues.name);
                    formData.append('category_id', formValues.category_id);
                    formData.append('description', formValues.description);
                    formData.append('price', formValues.price);
                    formData.append('weight', formValues.width);
                    formData.append('height', formValues.height);
                    formData.append('depth', formValues.depth);

                    let files = mapFilter(imgs, (e) => {
                        if (e.file !== undefined) return e.file;
                    });

                    formData.append('thumbnail', files[0]);
                    files.splice(0, 1);
                    files.forEach((e) => formData.append('images[]', e));

                    await method(path, formData, GlobalData['config_form']);
                    window.location.reload();
                }
            } type="submit" value="Enviar"/>
        </div>
    )
    //#endregion
}
