import './SeletorProdutos.css';

import React, { useState, useEffect } from 'react'
import { GlobalData } from '../../../../../VariableManager';
import { clamp } from '../../../../../Functions';
import Viradordepagina from '../../../../../components/OutrosComponentes/ViradorDePagina/ViradorDePagina';
import { fetchData } from '../../../../../ConnectionManager';

export default function SeletorProdutos(props) {
    const api = GlobalData['api'];

    const elementsPerPage = 5;

    const [actualPage, setActualPage] = useState(1);
    const [products, setProducts] = useState([]);

    const getProducts = () => fetchData('products', (e) => {
        setProducts(e.products.sort((a, b) => a.id - b.id));
    });

    useEffect(getProducts, []);

    //#region Build html

    let buildHtml = [];

    let start = elementsPerPage * (actualPage - 1);
    let end = clamp(start + elementsPerPage, 0, products.length);

    for (let i = start; i < end; i++) {
        const e = products[i];
        buildHtml.push(
            <div key={i} className="product-box">
                <p>{e.name}</p>
                <div>
                    <button onClick={
                        () => (props.editingProduct.id !== e.id) ? props.setEditingProduct(e) : props.setEditingProduct(-1)
                    }>Editar</button>
                    <button onClick={
                        () => api.delete('products/' + e.id, GlobalData['config'])
                        .then(getProducts)
                    }>Deletar</button>
                </div>
            </div>
        );
    }

    //#endregion

    const amountOfPages = Math.ceil(products.length/parseFloat(elementsPerPage));
    return (
        <section className="product-selector">
            <h3>Selecionar produto</h3>
            <div>
                {buildHtml}
            </div>
            <Viradordepagina actualPage={actualPage} setActualPage={setActualPage} maxPageToShow={elementsPerPage} totalAmountOfPages={amountOfPages}/>
        </section>
    )
}
