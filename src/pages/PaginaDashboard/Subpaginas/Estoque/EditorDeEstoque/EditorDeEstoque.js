import React, { useState } from 'react';
import './EditorDeEstoque.css'
import { GlobalData } from '../../../../../VariableManager';

export default function EditorDeEstoque(props) {
    const api = GlobalData['api'];

    const [amount, setAmount] = useState(0);

    return (
        <div className="stock-editor">
            <p>{(props.product <= 0) ? "Nada selecionado." : "Mudando valor de " + props.product.name}</p>
            <input defaultValue={props.product.storage} value={amount} onChange={(e) => setAmount(e.target.value)} type="number" min={0}/>
            <button onClick={
                () => {
                    if (isNaN(amount)) {
                        alert("Numero invalido.");
                        return;
                    }
                    api.put('products/' + props.product.id,
                        {
                            "storage": amount
                        }
                    , GlobalData['config']).then(props.getProducts);
                }
            }>Editar</button>
        </div>
    )
}
