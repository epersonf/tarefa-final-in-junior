import React, { useState, useEffect } from 'react';
import './Estoque.css';
import { GlobalData } from '../../../../VariableManager';
import EditorDeEstoque from './EditorDeEstoque/EditorDeEstoque';
import ViradorDePagina from '../../../../components/OutrosComponentes/ViradorDePagina/ViradorDePagina';
import { clamp, generateHtml } from '../../../../Functions';
import { fetchData } from '../../../../ConnectionManager';

export default function Estoque() {

    const elementsPerPage = 8;

    const [productList, setProductList] = useState([]);
    const [actualPage, setActualPage] = useState(1);
    const [selected, setSelected] = useState(-1);

    const getProducts = () => fetchData('products',
        (e) => 
            setProductList(
                e.products.sort((a, b) => a.id - b.id)
            )
        );
        

    useEffect(getProducts, []);

    let start = elementsPerPage * (actualPage - 1);
    let end = clamp(start + elementsPerPage, 0, productList.length);

    const amountOfPages = Math.ceil(productList.length/parseFloat(elementsPerPage));

    return (
        <section className="store">
            <div className="storage">
                {generateHtml(productList, (e) => {
                    return (
                        <div key={e.id} className="product" onClick={() => setSelected(e)}>
                            <p>{e.name}</p>
                            <p>{(e.storage > 0) ? "No estoque: " + e.storage : "Sem estoque :("}</p>
                        </div>
                    );
                }, start, end)}
            </div>
            <EditorDeEstoque product={selected} getProducts={getProducts}/>
            <ViradorDePagina actualPage={actualPage} setActualPage={setActualPage} maxPageToShow={elementsPerPage} totalAmountOfPages={amountOfPages}/>
        </section>
    )
}
