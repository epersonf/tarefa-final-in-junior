import React, { useState, useEffect } from 'react';
import './Usuarios.css';
import { GlobalData } from '../../../../VariableManager';
import ViradorDePagina from '../../../../components/OutrosComponentes/ViradorDePagina/ViradorDePagina';
import { clamp } from '../../../../Functions';
import { fetchData } from '../../../../ConnectionManager';

const href = "dashboard/users";

export default function Usuarios() {
    const api = GlobalData['api'];

    const elementsPerPage = 6;

    const [actualPage, setActualPage] = useState(1);
    const [users, setUsers] = useState([]);

    const getUsers = () => fetchData(href, (e) => {
        setUsers(e.sort((a, b) => a.id - b.id));
    });

    useEffect(getUsers, []);

    let buildTable = [];

    let start = elementsPerPage * (actualPage - 1);
    let end = clamp(start + elementsPerPage, 0, users.length);

    for (let i = start; i < end; i++) {
        const e = users[i];

        let adminText = (!e.admin) ? "Tornar admin" : "Remover admin";

        buildTable.push(
            <div key={e.id * 10} className="user-box">
                <p key={0}>{e.name}</p>
                <p key={1}>{e.email}</p>
                <p key={2}>{e.cpf}</p>
                <div className="buttons">
                    <button key={3} onClick={
                        () => {
                            if (e.id === GlobalData['user'].id) {
                                if (!window.confirm("Deseja mesmo tirar seus privilegios de administrador?")) {
                                    GlobalData['logged'] = false;
                                    document.location.href = '/';
                                    return;
                                }
                            }
                            api.put("dashboard/users/" + e.id + "/set_admin",
                            {
                                "user": {
                                    "admin": !e.admin
                                }
                            }
                            , GlobalData['config']).then(getUsers);
                        }
                    }>{adminText}</button>
                    <button key={4} onClick={
                        () => {
                            if (e.id === GlobalData['user'].id) {
                                if (!window.confirm("Deseja mesmo deletar sua conta?")) {
                                    GlobalData['logged'] = false;
                                    document.location.href = '/';
                                    return;
                                }
                            }
                            api.delete(href + "/" + e.id, GlobalData['config']).then(getUsers);
                        }
                    }>Deletar</button>
                </div>
            </div>
        );
    }
    
    const amountOfPages = Math.ceil(users.length/parseFloat(elementsPerPage));

    return (
        <section className="show-users">
            <h2>Usuarios cadastrados</h2>
            <div className="tables">
                {buildTable}
            </div>
            <ViradorDePagina actualPage={actualPage} setActualPage={setActualPage} maxPageToShow={elementsPerPage} totalAmountOfPages={amountOfPages}/>
        </section>
    )
}
