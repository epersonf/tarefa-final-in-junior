import React from 'react';
import './SideMenu.css';

export default function SideMenu(props) {
    const items = [
        "Produtos e Categorias",
        "Usuarios",
        "Estoque",
        "Administração"
    ];

    let uls = [];
    for (let i = 0; i < items.length; i++) {
        let clss = "";
        if (i === props.page) clss = "selected";
        uls.push(
            <ul key={i} onClick={() => props.setPage(i)} className={clss}>{items[i]}</ul>
        );
    }

    return (
        <div className="side-menu">
            <nav>
                {uls}
            </nav>
        </div>
    )
}
