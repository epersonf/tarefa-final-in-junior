import React, { useState } from 'react';
import './Registro.css';
import { GlobalData } from '../../VariableManager';
// import ComponenteDeEtapas from '../../components/OutrosComponentes/ComponenteDeEtapas/ComponenteDeEtapas';

export default function Registro(props) {
    //#region Caso alguem logado tente acessar essa pagina
    if (GlobalData['logged']) {
        document.location.href = '/';
    }
    //#endregion

    const [step, setStep] = useState(0);


    //#region States form 1
    const [nome, setNome] = useState("");
    const [email, setEmail] = useState("");
    const [senha, setSenha] = useState("");
    const [confirmaSenha, setConfirmaSenha] = useState("");
    //#endregion

    //#region States form 2
    const [cep, setCep] = useState("");
    const [rua, setRua] = useState("");
    const [numero, setNumero] = useState("");
    const [bairro, setBairro] = useState("");
    const [cidade, setCidade] = useState("");
    const [estado, setEstado] = useState("");
    //#endregion

    //#region States form 3
    const [fixo, setFixo] = useState("");
    const [celular, setCelular] = useState("");
    //#endregion

    const api = GlobalData['api'];

    //#region Requisicao de registro
    const tryToRegister = async () => {
        let response = await api.post('signup',
            {
                "user": {
                    "name": nome,
                    "email": email,
                    "password": senha,
                    "phone": fixo,
                    "cellphone": celular
                }
            },
        GlobalData['config'])
        .then((e)=>{
            api.post('addresses', 
            {
                "addresses": {
                    "postal_code": cep,
                    "street": rua,
                    "number": numero,
                    "district": bairro,
                    "city": cidade,
                    "federal_unity": estado,
                    "user_id": e.id,
                    "alias": "principal"
                }
            },
            GlobalData['config'])
        })
        .catch((e) => {
            alert("Erro ao criar conta: " + e)
        });
        if (response === undefined) return;
        

        document.location.href = '/';
        setStep(0);
    }
    //#endregion

    //#region Gerar forumlario
    const htmls = [
        <React.Fragment>
            <h1>Fazer cadastro</h1>
            <label>Nome</label>
            <input value={nome} onChange={(e) => setNome(e.target.value)} type="text"/>
            <label>Email</label>
            <input value={email} onChange={(e) => setEmail(e.target.value)} type="email"/>
            <label>Senha</label>
            <input value={senha} onChange={(e) => setSenha(e.target.value)} type="password"/>
            <label>Confirmar senha</label>
            <input value={confirmaSenha} onChange={(e) => setConfirmaSenha(e.target.value)} type="password"/>
            <input id="btnBege" type="button" value="Continuar" onClick={() => {
                    if (senha !== confirmaSenha || senha === "" || nome === "" || email === "") {
                        alert("Formulario incorreto!");
                        return;
                    }
                    setStep(step + 1);
                }
            } />
        </React.Fragment>,
        <React.Fragment>
            <h1>Fazer cadastro</h1>
            <label>CEP</label>
            <input value={cep} onChange={(e) => setCep(e.target.value)} name="cep" type="text"/>
            <label>Endereço</label>
            <input value={rua} onChange={(e) => setRua(e.target.value)} name="rua" type="text"/>
            <label>Número</label>
            <input value={numero} onChange={(e) => setNumero(e.target.value)} name="numero" type="number"/>
            <label>Bairro</label>
            <input value={bairro} onChange={(e) => setBairro(e.target.value)} name="bairro" type="text"/>
            <label>Cidade</label>
            <input value={cidade} onChange={(e) => setCidade(e.target.value)} name="cidade" type="text"/>
            <select value={estado} onChange={(e) => setEstado(e.target.value)} name="estados-brasil">
                <option value="AC">Acre</option>
                <option value="AL">Alagoas</option>
                <option value="AP">Amapá</option>
                <option value="AM">Amazonas</option>
                <option value="BA">Bahia</option>
                <option value="CE">Ceará</option>
                <option value="DF">Distrito Federal</option>
                <option value="ES">Espírito Santo</option>
                <option value="GO">Goiás</option>
                <option value="MA">Maranhão</option>
                <option value="MT">Mato Grosso</option>
                <option value="MS">Mato Grosso do Sul</option>
                <option value="MG">Minas Gerais</option>
                <option value="PA">Pará</option>
                <option value="PB">Paraíba</option>
                <option value="PR">Paraná</option>
                <option value="PE">Pernambuco</option>
                <option value="PI">Piauí</option>
                <option value="RJ">Rio de Janeiro</option>
                <option value="RN">Rio Grande do Norte</option>
                <option value="RS">Rio Grande do Sul</option>
                <option value="RO">Rondônia</option>
                <option value="RR">Roraima</option>
                <option value="SC">Santa Catarina</option>
                <option value="SP">São Paulo</option>
                <option value="SE">Sergipe</option>
                <option value="TO">Tocantins</option>
            </select>
            <input id="btnBege" type="button" value="Continuar" onClick={() => setStep(step + 1)} />
        </React.Fragment>,
        <React.Fragment>
            <h1>Fazer cadastro</h1>
            <h3>Telefones</h3>
            <label>Fixo:</label>
            <input value={fixo} onChange={(e) => setFixo(e.target.value)} type="text"/>
            <label>Celular:</label>
            <input value={celular} onChange={(e) => setCelular(e.target.value)} type="text"/>
            <input id="btnBege" type="button" value="Registrar" onClick={tryToRegister} />
        </React.Fragment>
    ];
    //#endregion

    return (
        <section className="register-sctn">
            {/* <ComponenteDeEtapas nStep={step} maxStep={htmls.length}/> */}
            <form>
                {htmls[step]}
            </form>
        </section>
    )
}
