import React, { useState } from 'react';
import './PaginaEditarPerfil.css';
import { GlobalData, ResetGlobalData } from '../../VariableManager';

export default function PaginaEditarPerfil() {

    const api = GlobalData['api'];
    const user = GlobalData['user'];

    const [name, setName] = useState(user.name);
    const [email, setEmail] = useState(user.email);
    const [password, setPsswrd] = useState(null);
    const [passwordCnfrm, setPsswrdCnfrm] = useState(null);
    const [telFixo, setTelFixo] = useState(user.phone);
    const [telCelular, setCelular] = useState(user.cellphone);

    const [addressType, setAddressType] = useState(user.alias);
    const [fullName, setFullName] = useState(user.receiver_name);
    const [cep, setCep] = useState(user.postal_code);
    const [address, setAddress] = useState(user.street);
    const [numberAddress, setNumberAddress] = useState(user.number);
    const [district, setDistrict] = useState(user.district);
    const [city, setCity] = useState(user.city);
    const [uf, setUf] = useState(user.federal_unity);

    const submit = () => {
        if (password != passwordCnfrm) {
            alert("Escreveu a senha errado...");
            return;
        }
        api.put('dashboard/users/' + user.id, {
            'user': {
                'name': name,
                'email': email,
                'password': password,
                'cellphone': telCelular,
                'phone': telFixo
            }
        }, GlobalData['config']).then((e) => {
            ResetGlobalData();
            alert('Perfil atualizado! Relogue para ver as mudancas...');
            document.location.href = "/";
        }).catch((e) => {
            alert('Por favor, insira sua senha.');
        });
    }

    return (
        <section className="edit-profile" action="" method="">
            <div>
                <h1>Editar perfil</h1>
                <button onClick={() => document.location.href = "/"} className="red-btn">Cancelar</button>
                <button onClick={submit} className="green-btn">Confirmar</button>
            </div>
            <section className="fields profile">
                <div>
                    <label htmlFor="nome">Nome</label>
                    <input value={name || ''} onChange={(e) => setName(e.target.value)} placeholder="Nome" type="text"/>
                </div>
                <div>
                    <label htmlFor="email">E-mail</label>
                    <input value={email || ''} onChange={(e) => setEmail(e.target.value)} placeholder="Email" type="email"/>
                </div>
                <div>    
                    <label htmlFor="senha">Senha</label>
                    <input value={password || ''} onChange={(e) => setPsswrd(e.target.value)} placeholder="Senha" type="password"/>
                </div>
                <div>
                    <label htmlFor="confirmar senha">Confirme sua selnha</label>
                    <input value={passwordCnfrm || ''} onChange={(e) => setPsswrdCnfrm(e.target.value)} placeholder="Senha" type="password"/>
                </div>
                <div>
                    <label htmlFor="telefone">Telefone fixo</label>
                    <input value={telFixo || ''} onChange={(e) => setTelFixo(e.target.value)} placeholder="Telefone Fixo" type="text"/>
                </div>
                <div>
                    <label htmlFor="celular">Telefone celular</label>
                    <input value={telCelular || ''} onChange={(e) => setCelular(e.target.value)} placeholder="Telefone Celular" type="text"/>
                </div>
            </section>
            <section className="fields address">
                <h3>Endereços:</h3>
                <div>
                    <label htmlFor="addressType">Como gostaria de salvar o endereço</label>
                    <input value={addressType || ''} onChange={(e) => setAddressType(e.target.value)} placeholder="Nome" type="text"/>
                </div>
                <div>
                    <label htmlFor="full name">Nome completo de quem vai receber</label>
                    <input value={fullName || ''} onChange={(e) => setFullName(e.target.value)} placeholder="Nome completo" type="text"/>
                </div>
                <div>    
                    <label htmlFor="CEP">CEP</label>
                    <input value={cep || ''} onChange={(e) => setCep(e.target.value)} placeholder="CEP" type="text"/>
                </div>
                <div>
                    <label htmlFor="address">Endereço</label>
                    <input value={address || ''} onChange={(e) => setAddress(e.target.value)} placeholder="Endereço" type="text"/>
                </div>
                <div className="fields-block">
                    <div>
                        <label htmlFor="number address">Número</label>
                        <input value={numberAddress || ''} onChange={(e) => setNumberAddress(e.target.value)} placeholder="Número" type="text"/>
                    </div>
                    <div>
                        <label htmlFor="district">Bairro</label>
                        <input value={district || ''} onChange={(e) => setDistrict(e.target.value)} placeholder="Bairro" type="text"/>
                    </div>
                    <div>
                        <label htmlFor="city">Cidade</label>
                        <input value={city || ''} onChange={(e) => setCity(e.target.value)} placeholder="Cidade" type="text"/>
                    </div>
                    <div>
                        <label htmlFor="district">Bairro</label>
                        <input value={uf || ''} onChange={(e) => setUf(e.target.value)} placeholder="Estado" type="text"/>
                    </div>
                </div>
            </section>
        </section>
    )
}
