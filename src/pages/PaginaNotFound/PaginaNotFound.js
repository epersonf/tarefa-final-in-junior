import React from 'react';
import './PaginaNotFound.css';
import errorSvg from './404.svg';

export default function PaginaNotFound() {
    return (
        <section className="page-not-found">
            <h1>Pagina nao encontrada.</h1>
            <img src={errorSvg} alt="Imagem mostrando engrenagem."/>
        </section>
    )
}
