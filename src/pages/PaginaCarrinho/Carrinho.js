import React, { useState } from 'react'
import './Carrinho.css';
import CartShow from './CartShow/CartShow';
import InfoCart from './InfoCart/InfoCart';

export default function Carrinho() {

    const [update, setUpdate] = useState(false);

    return (
        <section className="cart-section">
            <CartShow update={() => setUpdate(!update)}/>
            <InfoCart update={() => setUpdate(!update)}/>
        </section>
    )
}
