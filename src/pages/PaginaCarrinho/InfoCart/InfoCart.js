import React, { useState } from 'react';
import './InfoCart.css';
import { GlobalData, BakeGlobalData } from '../../../VariableManager';

export default function InfoCart(props) {
    
    const clearCart = () => {
        GlobalData['cart'] = [];
        props.update();
        BakeGlobalData();
    }

    let calculateTotal = () => {
        let sum = 0;
        GlobalData['cart'].forEach((e) => {
            console.log(e.product);
            sum += parseFloat(e.product.price) * parseFloat(e.amount);
        });
        return sum.toFixed(2);
    }

    return (
        <section className='info-cart'>
            <p>Valor total: R$ {calculateTotal()}</p>
            <button className="red-btn" onClick={clearCart}>Limpar carrinho</button>
            <button className="green-btn">Comprar agora</button>
        </section>
    )
}
