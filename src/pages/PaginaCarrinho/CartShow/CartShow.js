import React, { useState } from 'react';
import './CartShow.css';
import { GlobalData, BakeGlobalData } from '../../../VariableManager';
import { generateHtml } from '../../../Functions';

export default function CartShow(props) {
    let cart = GlobalData['cart'];

    const addAmount = (add, index) => {
        cart[index].amount += add;
        if (cart[index].amount <= 0)
            removeFromCart(undefined, index);
        props.update();
        BakeGlobalData();
    }
    
    const removeFromCart = (e, index) => {
        cart.splice(index, 1);
        props.update();
        BakeGlobalData();
    }

    return (
        <div className='chart-box'>
            
            <h3>{cart.length <= 0 ? "Não há nada neste carrinho!" : ""}</h3>
            {generateHtml(cart, (e, index) => {
                return (
                    <div key={e.product.id} className='product-box'>
                        <h3>{e.product.name}</h3>
                        <div className="amount-changer">
                            <button id="red-btn" onClick={() => addAmount(-1, index)}>-</button>
                            <p>{e.amount}</p>
                            <button id="green-btn" onClick={() => addAmount(1, index)}>+</button>
                        </div>
                        <div>
                            <p>Valor unitário: R$ {parseFloat(e.product.price).toFixed(2)}</p>
                            <p>Valor total: R$ {parseFloat(e.product.price * e.amount).toFixed(2)}</p>
                        </div>
                        <i onClick={() => removeFromCart(e, index)} className="trash-icon fas fa-trash"></i>
                    </div>
                );
            })}
        </div>
    )
}
