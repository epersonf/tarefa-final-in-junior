import { GlobalData } from "./VariableManager";

export const fetchData = (path, callback) => {
    const fetch = async () => {
        const response = await GlobalData['api'].get(path, GlobalData['config']);
        const responseData = response.data;
        callback(responseData);
    }
    fetch();
}