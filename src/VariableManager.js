import React from 'react'
import App from './App';
import axios from 'axios';

let api = 
axios.create(
  {
      baseURL: 'https://shopp-in-g.herokuapp.com/'
  }
);

const config = {
    headers: {
      'content-type': 'application/json'
    }
};

const configForm = {
    headers: {
      'content-type': 'multipart/form-data'
    }
};



const cookieName = "ShoppingGlobalData";

export var GlobalData = JSON.parse(window.localStorage.getItem(cookieName));


export function ResetGlobalData() {
  GlobalData = {};
  GlobalData['cart'] = [];
  GlobalData['config'] = config;
  GlobalData['config_form'] = configForm;
  GlobalData['logged'] = false;
  GlobalData['user'] = {
    'admin': false
  };
  console.log("Reset GlobalData.");
  BakeGlobalData();
}

if (GlobalData === null || GlobalData === '') {
  ResetGlobalData();
}

GlobalData['api'] = api;

export function BakeGlobalData() {
  window.localStorage.setItem(cookieName, JSON.stringify(GlobalData));
  console.log("GlobalData baked.");
}

export default function VariableManager() {
    return (
        <App />
    )
}