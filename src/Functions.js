// Aqui serao colocadas funcoes para utilidades

export const clamp = function(actual, min, max) {
    if (actual > max) return max;
    if (actual < min) return min;
    return actual;
};

export const generateRandomColor = () => {
    return 'rgba(' + getRandomInt(0, 255) + ', ' + getRandomInt(0, 255) + ' , ' + getRandomInt(0, 255) +', ' + clamp(Math.random(), .7, 1) + ')';
}

export const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

export const generateHtml = (input, jsxToReturn, start, end) => {
    if (start === undefined) start = 0;
    if (end === undefined) end = input.length;
    
    let html = [];
    for (let i = start; i < end; i++) html.push(jsxToReturn(input[i], i));

    return html;
}

export const mapFilter = (inputArray, method) => {
    let build = [];

    inputArray.forEach((e) => {
        let newValue = method(e);
        if (newValue !== undefined) {
            build.push(newValue);
        }
    });

    return build;
}