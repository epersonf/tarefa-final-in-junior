import React from 'react';
import './Footer.css';

export default function Footer() {
    return (
        <footer>
            <div className="column-footer">
                <div className="logos-footer">
                    <i onClick={() => document.location = "https://www.instagram.com"} className="icon fab fa-instagram"></i>
                    <i onClick={() => document.location = "https://www.facebook.com"} className="icon fab fa-facebook-square"></i>
                    <i onClick={() => document.location = "https://www.twitter.com"} className="icon fab fa-twitter-square"></i>
                </div>
                <p>&#169; Copyright 2020 Shopp{"{IN}"}g</p>
            </div>
            <p>Desenvolvido pelos trainees: Eperson, Letícia e Wallace</p>
        </footer>
    )
}
