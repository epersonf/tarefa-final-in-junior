import React, { useEffect } from 'react';
import './Header.css';
import Logo_laco_simetrico from './Logo_laco_simetrico.svg';
import { GlobalData, BakeGlobalData } from '../../../VariableManager';

export default function Header(props) {
    const goToHomePage = () => {
        if (document.location.href === document.location.origin + '/') return;
        document.location.href = '/';
    }

    const logout = () => {
        GlobalData['logged'] = false;
        GlobalData['user'] = '';
        GlobalData['config'] = {
            headers: {
            accept: 'application/json'
            },
            data: {},
        };
        document.location.href = '/';
        BakeGlobalData();
    };

    const headerType = [
        <React.Fragment>
                <a href="/produtos">Produtos</a>
                <a href="/login">Login</a>
                <a href="/registro">Cadastro</a>
        </React.Fragment>,
        <React.Fragment>
                <a href="/produtos">Produtos</a>
                <a href="/perfil">Perfil</a>
                <a onClick={logout} href="/">Sair</a>
        </React.Fragment>,
        <React.Fragment>
                <a href="/produtos">Produtos</a>
                <a href="/perfil">Perfil</a>
                <a onClick={logout} href="/">Sair</a>
                <button onClick={() => {document.location.href = '/dashboard';}}>Dashboard</button>
        </React.Fragment>
    ];

    let index = 0;
    try {
        if (GlobalData['user'].admin) {
            index = 2;
        } else if(GlobalData['logged']) {
            index = 1;
        }
    } catch(e) {
        //isso significa que o usuario nao esta logado
    }

    useEffect(BakeGlobalData, []);

    return (
        <header className={index === 2 ? "admin" : ""}>
            <img onClick={goToHomePage} src={Logo_laco_simetrico} alt="Logo SHOPP{IN}G"/>
            
            <nav className="navbar">
                <i onClick={() => document.location.href = '/carrinho'}className="cart-icon fas fa-shopping-cart"></i>
                {headerType[index]}
            </nav>
        </header>
    )
}
