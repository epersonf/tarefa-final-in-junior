import React from 'react';
import './ComponenteDeEtapas.css';

export default function ComponenteDeEtapas(props) {

    const colors = ["#333", "#115511"];

    let structure = [];
    for (let i = 0; i < props.maxStep; i++) {
        console.log(colors[+(i > props.step)]);
        structure.push(
            <div key={i} className="box-step" style={{backgroundColor: colors[+(i > props.step)]}}>
                
            </div>
        );
    }

    return (
        <div className="step-counter">
            {structure}
        </div>
    )
}
