import React, { useState, useEffect } from 'react';
import { fetchData } from '../../../ConnectionManager';
import { generateHtml } from '../../../Functions';

export default function CategorySelector(props) {

    const [categories, setCategories] = useState([]);

    const getCategories = () => fetchData('categories', (e) => {
        setCategories(e);
    });

    useEffect(getCategories, []);

    return (
        <select value={props.value} className="category-select" onChange={(e) => props.setCategory(e.target.value)}>
            <option value={-1}>Nada</option>
            {generateHtml(categories, (e) => (
                <option value={e.id} key={e.id}>{e.name}</option>
            ))}
        </select>
    )
}
