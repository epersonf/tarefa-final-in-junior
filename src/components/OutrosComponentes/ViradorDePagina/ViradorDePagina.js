import React from 'react';
import './ViradorDePagina.css';
import { clamp } from '../../../Functions';

export default function ViradorDePagina(props) {
    let minimumPage = clamp(props.actualPage - parseInt(props.maxPageToShow/2), 1, props.totalAmountOfPages);
    let maximumPage = clamp(props.actualPage + parseInt(props.maxPageToShow/2), 1, props.totalAmountOfPages);

    let buttons = [];
    for (let i = minimumPage; i <= maximumPage; i++) {
        let style = "";
        if (props.actualPage === i) style = "bold-pg-btn";

        buttons.push(
            <p key={i} className={"page-btn " + style} onClick={
                (e) => {
                    if (i === props.actualPage) return;
                    props.setActualPage(i);
                }
            }>
                {i}
            </p>
        );
    }

    return (
        <div className="page-buttons">
            {buttons}
        </div>
    )
}

