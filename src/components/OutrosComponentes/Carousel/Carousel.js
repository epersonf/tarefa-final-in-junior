import React, { useState } from 'react';
import './Carousel.css';
import { clamp } from '../../../Functions';

export default function Carousel(props) {

    const [id, setId] = useState(0);

    if (props.imgs === undefined) return;
    
    return (
        <div className="carousel">
            {<img src={props.imgs[id]?.url} alt={"Imagem " + id}/>}
            <div className='buttons-carousel-controller'>
                <button className="carousel-controller-btn"onClick={() => setId(clamp(id - 1, 0, props.imgs.length - 1))}>Left</button>
                <button className="carousel-controller-btn" onClick={() => setId(clamp(id + 1, 0, props.imgs.length - 1))}>Right</button>
            </div>
        </div>
    );
}
