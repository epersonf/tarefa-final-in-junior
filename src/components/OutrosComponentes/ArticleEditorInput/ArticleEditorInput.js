import React, { useState, useEffect } from 'react';
import { GlobalData } from '../../../VariableManager';
import './ArticleEditorInput.css';
import ImageBox from '../../../pages/PaginaDashboard/Subpaginas/ProdutosECategorias/ProdutoForm/ImageBox/ImageBox';
import { fetchData } from '../../../ConnectionManager';

export default function ArticleEditorInput(props) {
    const api = GlobalData['api'];
    
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');
    const [img, setImage] = useState([]);

    const addImage = (e) => {
        const file = e.target.files[0];
        const url = window.URL.createObjectURL(file);
        setImage(
            [
                {
                    'id': -1,
                    'file': file,
                    'url': url
                }
            ]
        );
    }

    const updateForm = () => fetchData('articles/' + props.articleId, (e) => {
        const article = e.article;
        setTitle(article.title);
        setContent(article.content);
        setImage(article.images);
    })

    useEffect(updateForm, [props.articleId]);

    const generateForm = () => {
        let formData = new FormData();
        formData.append('title', title);
        formData.append('content', content);

        if (img[0] != undefined || img[0].id !== -1) {
            formData.append('images[]', img[0].file);
        }
        api.put('articles/' + props.articleId, formData, GlobalData['config_form']).then(alert('Editado com sucesso!'));
    }

    return (
        <div className='article-editor-input'>
            <input type="text" value={title || ''} onChange={(e) => setTitle(e?.target.value)}/>
            <textarea value={content || ''} onChange={(e) => setContent(e?.target.value)}></textarea>
            <input id="img-add-article-btn" type="file" onChange={(e) => addImage(e)}/>
            {img.map((e) => {
                return (<img key={e.id} src={e.url} alt={"Imagem " + e.id} />);
            })}
            <button onClick={generateForm}>Editar</button>
        </div>
    )
}
