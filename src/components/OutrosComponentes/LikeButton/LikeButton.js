import React from 'react'
import './LikeButton.css';
import { GlobalData } from '../../../VariableManager';


export default function LikeButton(props) {
    const api = GlobalData['api'];

    const clickLike = () => {
        const path = props.path;
        //not implemented yet
    }

    return (
        <div className="div-like-btn">
            <i onClick={clickLike} className="like-btn fas fa-thumbs-up"></i>
            <p> {props.likeCount} likes</p>
        </div>
    )
}
