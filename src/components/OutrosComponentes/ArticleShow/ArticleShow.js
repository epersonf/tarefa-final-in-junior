import React, { useState, useEffect } from 'react';
import './ArticleShow.css';
import { fetchData } from '../../../ConnectionManager';

export default function ArticleShow(props) {

    const [json, setJson] = useState([]);

    const getArticle = () => fetchData('articles/' + props.id, (e) => {
        setJson(e.article);
    });

    useEffect(getArticle, []);

    if (json.images === undefined) return('');

    return (
        <section className="sct-sobre-nos">
            <h3>{json.title}</h3>
            <div className="flex-objects">
                <p>{json.content}</p>
                <img src={json.images[0].url} alt="Imagem sobre a empresa"></img>
            </div>
        </section>
    )
}
