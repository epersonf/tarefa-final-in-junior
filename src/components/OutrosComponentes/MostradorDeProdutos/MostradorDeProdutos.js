import React, { useState, useEffect } from 'react';
import './MostradorDeProdutos.css';
import { clamp, generateHtml } from '../../../Functions';
import { fetchData } from '../../../ConnectionManager';

export default function MostradorDeProdutos(props) {

    //#region Aqui sera feita a requisicao Ajax
    const [products, setProducts] = useState([]);

    let buildString = 'products?limit=' + props.elementsPerPage + '&page=' + props.actualPage;
    if (props.filter) {
        if (props.filter.category_id !== -1) buildString += '&category_id=' + props.filter.category_id;
        if (props.filter.name !== '') buildString += '&search=' + props.filter.name;
        if (props.filter.min_price !== 0) buildString += '&above=' + props.filter.min_price;
        if (props.filter.max_price !== 0) buildString += '&below=' + props.filter.max_price;
    }

    const getProducts = () => fetchData(buildString, (i) => {
        let e = i.products;
        if (props.sortMethod != null) {
            e = e.sort(props.sortMethod);
        }
        setProducts(e);

        if (props.setProductsAmount === undefined) return;
        props.setProductsAmount(i.total_result);
    });

    useEffect(getProducts, [props.actualPage, props.filter]);
    //#endregion

    return (
        <section className="show-products">
            {generateHtml(products, (e) => {
                if (e === undefined) return;
                return (
                    <div onClick={() => document.location.href = "produto/" + e.id} key={e.id} className="productBox">
                        <h4>{e.name}</h4>
                        <img src={e.thumbnail?.url} alt={"Imagem de " + e.name}></img>
                        <p>R$ {e.price}</p>
                    </div>
                );
            })}
        </section>
    )
}
