import React, { useState, useEffect } from 'react'
import { fetchData } from '../../../ConnectionManager';

export default function ArticleSelector(props) {

    const [articles, setArticles] = useState([]);

    const getArticles = () => fetchData('articles', (e) => setArticles(e));

    useEffect(getArticles, []);

    return (
        <select onChange={(e) => props.changeCallBack(e.target.value)}>
            {articles.map(e => {
                return (<option key={e.id} value={e.id}>{e.name}</option>)
            })}
        </select>
    )
}
