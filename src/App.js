import React from 'react';
import './App.css';
import { Route, Switch } from "react-router";
import { BrowserRouter } from 'react-router-dom';

//#region Import de defaults

import Header from './components/PageDefaults/Header/Header';
import Footer from './components/PageDefaults/Footer/Footer';

//#endregion

//#region Import de paginas

import Carrinho from "./pages/PaginaCarrinho/Carrinho";
import Dashboard from "./pages/PaginaDashboard/Dashboard";
import PaginaInicial from "./pages/PaginaInicial/PaginaInicial";
import PaginaLogin from "./pages/PaginaLogin/Login";
import PaginaProdutos from "./pages/PaginaProdutos/Produtos";
import ProdutosSingle from "./pages/PaginaProdutosSingle/ProdutosSingle";
import PaginaRegistro from "./pages/PaginaRegistro/Registro";
import PaginaNotFound from "./pages/PaginaNotFound/PaginaNotFound";
import PaginaEditarPerfil from './pages/PaginaEditarPerfil/PaginaEditarPerfil';
import PaginaPagamentos from './pages/PaginaPagamentos/PaginaPagamentos';

//#endregion


function App() {
  return (
    <div className="App">

      <Header />

      <main>
        <BrowserRouter>
          <Switch>

            {/* PAGINA INICIAL */}
            <Route exact path = "/" component={PaginaInicial} />

            {/* PAGINA PRODUTOS */}
            <Route exact path = "/produtos" component={PaginaProdutos} />

            {/* PRODUTO SINGLE */}
            <Route path = "/produto/" component={ProdutosSingle} />

            {/* LOGIN */}
            <Route exact path = "/login" component={PaginaLogin} />

            {/* REGISTRO */}
            <Route exact path = "/registro" component={PaginaRegistro} />

            {/* DASHBOARD */}
            <Route exact path = "/dashboard" component={Dashboard} />
            
            {/* PAGINA CARRINHO */}
            <Route exact path = "/carrinho" component={Carrinho} />

            {/* PAGINA EDITAR PERFIL */}
            <Route exact path = "/perfil" component={PaginaEditarPerfil} />

            {/* PAGINA PAGAMENTOS */}
            <Route exact path = "/pagar" component={PaginaPagamentos} />

            {/* PAGINA 404 */}
            <Route component={PaginaNotFound} />

          </Switch>
        </BrowserRouter>
      </main>

      <Footer />
    </div>
  );
}

export default App;
